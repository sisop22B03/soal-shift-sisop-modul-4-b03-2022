#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>

char *anime = "Animeku_";
char *ian = "IAN_";
char *key = "INNUGANTENG";
char *namdosaq = "nam_do-saq_";
static const char *dirpath = "/home/archblaze/";

void encodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, 0);

    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A';
            else continue;

            temp = 25 - temp;

            if(isupper(path[i]))
                temp += 'A';

            path[i] = temp;
        }
    }
}

void decodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, end);

    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A';
            else continue;

            temp = 25 - temp;

            if(isupper(path[i]))
                temp += 'A';

            path[i] = temp;
        }
    }
}

void encodeRot13(char *path)
{
 if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, 0);

 for (int i = begin; i < end; i++){
  if (path[i] != '/' && isalpha(path[i])){
   char temp = path[i];
   if(isupper(path[i]))
                      continue;
            else
                temp -= 'a';

            temp = (temp + 13) % 26;

            if(isupper(path[i]))
                continue;
            else temp += 'a';

            path[i] = temp;
  }
 }
}

void decodeRot13(char *path)
{
 if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, end);

 for (int i = begin; i < end; i++){
  if (path[i] != '/' && isalpha(path[i])){
   char temp = path[i];
   if(isupper(path[i]))
                continue;
            else temp -= 'a';

            temp = (temp + 13) % 26;

            if(isupper(path[i])) continue;
            else
                temp += 'a';

            path[i] = temp;
  }
 }
}

int dotFind(char *path)
{
    int len = strlen(path);
    for(int i = len-1; i >= 0; i--)
    {
        if(path[i] == '.')
        {
            return i;
        }
    }
    return len;
}

int slashfind(char *path, int offset)
{
    int len = strlen(path);
    for(int i = 0; i < len; i++)
    {
        if(path[i] == '/')
        {
            return i + 1;
        }
    }
    return offset;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeRot13(satu);
        decodeAtbash(satu);
    }
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (satu != NULL) {
            encodeAtbash(de->d_name);
            encodeRot13(de->d_name);
        }

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;

    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

	res = mkdir(path, mode);

    if(satu != NULL) writeLog("MKDIR terenkripsi", fpath, "", 1);
    else writeLog("MKDIR", fpath, "", 1);


	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    char *satu = strstr(to, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    sprintf(fromDir, "%s%s", dirpath, from);
    sprintf(toDir, "%s%s", dirpath, to);

    if(strstr(toDir, anime) != NULL) writeLog("RENAME terenkripsi", fromDir, toDir, 2);
    else if(strstr(fromDir, anime) != NULL) writeLog("RENAME terdecode", fromDir, toDir, 2);

    if (dua != NULL || strstr(from, ian) != NULL) writeLogSequel("RENAME", fromDir, toDir, 2);

	res = rename(fromDir, toDir);
	if (res == -1)
		return -errno;

	return 0;
}


static struct fuse_operations xmp_oper = 
{
 .getattr = xmp_getattr,
 .readdir = xmp_readdir,
 .read = xmp_read,
 .mkdir = xmp_mkdir,
 .rename = xmp_rename,
};



int  main(int  argc, char *argv[])
{
    umask(0007);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
