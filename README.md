# Soal Shift Sisop Modul 4 B03 2022

# Laporan Resmi

Ini adalah laporan resmi Modul 3 untuk mata kuliah Sistem Operasi tahun 2022.

**Kelas**: B<br>
**Kelompok**: B03<br>
**Modul**: 4<br>

## Anggota

- Muhamad Ridho Pratama (5025201186)
- Naily Khairiya (5025201244)
- Beryl (5025201029)

## Soal

Soal dapat dilihat di
https://drive.google.com/drive/u/0/folders/1wsKTz8WLPNCCW5lcJ55gKCJM-vkXufIO

## Dokumentasi

Berikut adalah dokumentasi, cara pengerjaan, dan kendala selama pengerjaan.

### Screenshot

Jika gambar tidak muncul, buka *link* Google Drive pada [README](./README.md)

#### **Soal 1**

#### **Soal 2**

#### **Soal 3**

### Cara Pengerjaan
1. a.	Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13

- membuat fungsi `encodeAtBash` dengan passingkan directory. Apabila memenehui syarat maka directory akan direname dengan fungsi ini

```c
void encodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, 0);

    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A';
            else continue;

            temp = 25 - temp;

            if(isupper(path[i]))
                temp += 'A';

            path[i] = temp;
        }
    }
}

```

- membuat fungsi encodeRot3 untuk huruf kecil semua

```c

void encodeRot13(char *path)
{
 if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, 0);

 for (int i = begin; i < end; i++){
  if (path[i] != '/' && isalpha(path[i])){
   char temp = path[i];
   if(isupper(path[i]))
                      continue;
            else
                temp -= 'a';

            temp = (temp + 13) % 26;

            if(isupper(path[i]))
                continue;
            else temp += 'a';

            path[i] = temp;
  }
 }
}

```

1b. b.	Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
- menggunakan xmp_rename untuk merename sesuai dengan ketentuan
```c

static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    char *satu = strstr(to, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    sprintf(fromDir, "%s%s", dirpath, from);
    sprintf(toDir, "%s%s", dirpath, to);

    if(strstr(toDir, anime) != NULL) writeLog("RENAME terenkripsi", fromDir, toDir, 2);
    else if(strstr(fromDir, anime) != NULL) writeLog("RENAME terdecode", fromDir, toDir, 2);

    if (dua != NULL || strstr(from, ian) != NULL) writeLogSequel("RENAME", fromDir, toDir, 2);

	res = rename(fromDir, toDir);
	if (res == -1)
		return -errno;

	return 0;
}
```
- membuat fungsi untuk decode
```c
void decodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, end);

    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A';
            else continue;

            temp = 25 - temp;

            if(isupper(path[i]))
                temp += 'A';

            path[i] = temp;
        }
    }
}


void decodeRot13(char *path)
{
 if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = dotFind(path);
    int begin = slashFind(path, end);

 for (int i = begin; i < end; i++){
  if (path[i] != '/' && isalpha(path[i])){
   char temp = path[i];
   if(isupper(path[i]))
                continue;
            else temp -= 'a';

            temp = (temp + 13) % 26;

            if(isupper(path[i])) continue;
            else
                temp += 'a';

            path[i] = temp;
  }
 }
}

```

